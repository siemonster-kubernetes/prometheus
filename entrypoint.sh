#!/bin/sh

/bin/confd -backend="env" -confdir="/etc/confd" -onetime  --log-level=debug

chown -R 65534:65534 /prometheus
gosu nobody:nogroup "/bin/prometheus" "$@"
